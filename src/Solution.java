import java.util.HashMap;
import java.util.Map;

public class Solution {
    public static void main(String[] args) {
        Solution solution = new Solution();
        solution.runTheApp();
    }

    private void runTheApp() {
        int nums [] = new int[]{2,7,11,15};
        int target = 9;
        int nums2 [] =  new int[]{3,2,4};
        int target2 = 6;
        int nums3 [] =  new int[]{2,5,5,11};
        int target3 = 10;
        twoSum(nums3, target3);
    }

    //best solution
    private int[] twoSum(int[] nums, int target) {
        Map<Integer, Integer> map = new HashMap<>();
        for(int i = 0; i < nums.length; i++) {
            int num = nums[i];
            if (map.containsKey((target - num))) {
                return new int[] {map.get(target - num), i};
            }
            map.put(num, i);
        }
        return new int[2];
    }

    /*private int[] twoSum(int[] nums, int target) {
        int [] result = new int[2];
        for (int i = 0; i < nums.length-1; i++) {
            int pos = i+1;
            while (pos<nums.length){
                if (nums[i]+nums[pos]==target){
                    result[0]=i;
                    result[1]=pos;
                }
                pos++;
            }
        }
        return result;
    }*/

    /*private int[]twoSum(int[] nums, int target) {
        for (int i = 0; i < nums.length; i++) {
            for (int j = i + 1; j < nums.length; j++) {
                if (nums[j] + nums[i] == target) {
                    int[] result = {i, j};
                    return result;
                }
            }
        }
        return null;
    }*/
}

